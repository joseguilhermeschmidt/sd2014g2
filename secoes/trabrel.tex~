\section{Sistemas Distribuídos} 
\label{sec:trabrel}



Um sistema distribuido, de acordo com ~\cite{Tanenbaum}, é uma coleção de computadores independentes que aparece para os usuários do sistema como um único computador. Levando em consideração a definição dada pelo Tanenbaum, sistema distribuido é aquele que roda em um conjunto de máquinas sem memória compartilhada e mesmo assim aparece para os usuários como se fosse um único computador.

Os sistemas distribuídos permitem que uma aplicação seja dividida em diferentes partes que se comunicam através de linhas de comunicação onde cada parte pode ser processada em um sistema independente.

O objetivo de um sistema distribuído, segundo ~\cite{Guedes}, é criar a ilusão que a aplicação (ou as aplicações) estão sendo processadas em um único sistema, permitindo a sensação que tudo isso ocorre sem o compartilhamento de áreas de memória, no entanto, a sincronização é feita a partir de trocas de mensagens. 

Faz parte do objetivo a situação da aplicação ser processada de modo que o ambiente que opera forneça situações favoráveis ao compartilhamento de recursos, sabendo que diferentes recursos estarão disponíveis em unidades de processamento diferentes.

Os Sistemas Distribuídos oferecem diversas vantagens, entre elas o compartilhamento de recursos, economia, eficiência, distribuição inerente, confiabilidade e crescimento incremental.


