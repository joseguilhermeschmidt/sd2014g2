package rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface ReceberMSGInterface extends Remote
{
    void receiveMessage(String x) throws RemoteException;
}
