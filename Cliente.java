package rmi;

import java.rmi.*;
import java.rmi.registry.*;

public class Cliente
{
    static public void main(String args[])
    {
       ReceberMSGInterface rmiServer;
       Registry registry;
       String serverAddress="127.0.0.1";
       int serverPort=1234;
       String text="teste";
       System.out.println("sending "+text+" to "+serverAddress+":"+serverPort);
       
       try{
           registry=LocateRegistry.getRegistry(
               serverAddress,
               serverPort
           );
           rmiServer = (ReceberMSGInterface)(registry.lookup("Servidor RMI"));
           rmiServer.receiveMessage(text);
       }
       catch(RemoteException | NotBoundException e){
       }
    }
}
