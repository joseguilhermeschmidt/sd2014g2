package rmi;

import java.rmi.*;
import java.rmi.registry.*;
import java.net.*;

public class Servidor extends java.rmi.server.UnicastRemoteObject implements ReceberMSGInterface {

    int Porta;
    String Endereco;
    Registry Registro;

    @Override
    public void receiveMessage(String x) throws RemoteException {
        System.out.println(x);
    }

    public Servidor() throws RemoteException {
        Endereco = (InetAddress.getLocalHost()).toString();
		Porta = 1234;
        Registro = LocateRegistry.createRegistry(Porta);
        Registro.rebind("Servidor RMI", this);
    }
}